#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "test/Test.h"
#include "Renderer.h"

#include <vector>
#include <unordered_map>


namespace test {

	class ImguiTestWindow {
	public:
		ImguiTestWindow(GLFWwindow* window, bool callbacks, const char* glsl_version, Renderer& renderer);
		~ImguiTestWindow();

		void testWindowNewFrame();
		void testWindowRender();
		void testMainComponents();
		void displayTest();
		[[nodiscard]] int getCurrentListItem();

	private:
		void addTestClass();
		void setDefault();

	private:
		const char* items[6];
		const char* current_item = NULL;

		std::vector<Test*> testsVector;
		Renderer m_Renderer;

		template <class Type>
		Test* createClass() {
			return new Type;
		}
		
		
	};

}