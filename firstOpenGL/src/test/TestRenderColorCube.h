#pragma once
#include "test/Test.h"
#include "VertexDefinition.h"
#include "Renderer.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h""
#include "IndexBuffer.h"
#include "Shader.h"


namespace test {

	class TestRenderColorCube : public Test {
	public:
		TestRenderColorCube();
		~TestRenderColorCube();

		void OnUpdate(float deltaTime) override;
		void OnRender(Renderer& renderer) override;
		void OnImGuiRender() override;

	private:
		float vertex[48] = {
			   -50.0f, -50.0f, 50.0f, 1.0f, 0.0f, 0.0f, // top left
				50.0f, -50.0f, 50.0f, 0.0f, 1.0f, 0.0f, // top right
				50.0f,  50.0f, 50.0f, 0.0f, 0.0f, 1.0f, // bottom right
			   -50.0f,  50.0f, 50.0f, 1.0f, 1.0f, 0.0f, // bottom left

			   -50.0f, -50.0f, -50.0f, 1.0f, 0.0f, 1.0f, // top left
				50.0f, -50.0f, -50.0f, 0.0f, 1.0f, 1.0f, // top right
				50.0f,  50.0f, -50.0f, 1.0f, 0.5f, 0.0f, // bottom right
			   -50.0f,  50.0f, -50.0f, 1.0f, 0.5f, 0.5f, // bottom left
		};

		unsigned int indicies[36]{
			 0,1,3,
			 3,2,1, // front

			 4,5,7,
			 7,6,5, // back

			 4,5,0,
			 0,1,5, // top

			 7,6,3,
			 3,2,6, // bottom

			 0,4,3,
			 3,7,4, // left

			 1,5,2,
			 2,6,5, // right
		};

		float m_ClearColor[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float m_rotation[3] = { 1.0f,1.0f,1.0f };
		bool m_activeRotation[3] =  { false, false, false };

		glm::mat4* m_proj;
		glm::mat4* m_view;
		glm::vec3* m_modelTranslation;

		VertexArray* va;
		VertexBuffer* vb;
		VertexBufferLayout* layout;
		IndexBuffer* ib;
		Shader* shader;

	public:
		float W_HEIGHT = 540.0f;
		float W_WIDTH = 960.0f;

	};
}
