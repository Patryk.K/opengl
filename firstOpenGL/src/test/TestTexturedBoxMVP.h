#pragma once
#include "test/Test.h"
#include "VertexDefinition.h"
#include "Renderer.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h""
#include "IndexBuffer.h"
#include "Shader.h"
#include "Texture.h"


namespace test {

	class TestTexturedBoxMVP : public Test {
	public:
		TestTexturedBoxMVP();
		~TestTexturedBoxMVP();

		void OnUpdate(float deltaTime) override;
		void OnRender(Renderer& renderer) override;
		void OnImGuiRender() override;

	private:
		vertex2D vertex[16] = {
			-50.0f, -50.0f, 0.0f, 0.0f,
			 50.0f, -50.0f, 1.0f, 0.0f,
			 50.0f,  50.0f, 1.0f, 1.0f,
			-50.0f,  50.0f, 0.0f, 1.0f,

		};
		unsigned int indicies[6]{
			 0,1,2,
			 2,3,0
		};

		glm::mat4* m_proj;
		glm::mat4* m_view;
		glm::vec3* m_modelTranslation;

		VertexArray* va;
		VertexBuffer* vb;
		VertexBufferLayout* layout;
		IndexBuffer* ib;
		Shader* shader;
		Texture* texture;

	public:
		float W_HEIGHT = 540.0f;
		float W_WIDTH = 960.0f;

	};
}