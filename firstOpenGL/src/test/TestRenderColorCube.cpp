#include "test/TestRenderColorCube.h"
#include "imgui/imgui.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

namespace test {


	TestRenderColorCube::TestRenderColorCube()
	{
		va = new VertexArray();
		vb = new VertexBuffer(vertex, sizeof(vertex));
		layout = new VertexBufferLayout();
		layout->Push<float>(3);
		layout->Push<float>(3);
		va->AddBuffer(*vb, *layout);

		ib = new IndexBuffer(indicies, 36);
		shader = new Shader("res/shaders/BasicColorVertexCube.shader");
		shader->Bind();

		m_proj = new glm::mat4(glm::ortho(0.0f, W_WIDTH, 0.0f, W_HEIGHT, -100.0f, 100.0f));
		m_view = new glm::mat4(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0)));

		m_modelTranslation = new glm::vec3(W_WIDTH / 2, W_HEIGHT / 2, 0.0f);

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	TestRenderColorCube::~TestRenderColorCube()
	{
		delete va;
		delete vb;
		delete layout;
		delete ib;
		delete shader;

		delete m_proj;
		delete m_view;
		delete m_modelTranslation;
	}

	void TestRenderColorCube::OnUpdate(float deltaTime)
	{
		glm::mat4 m_model = glm::translate(glm::mat4(1.0f), *m_modelTranslation);
		m_model = glm::rotate(m_model, glm::radians(m_rotation[0]), glm::vec3(1.0, 0.0, 0.0));
		m_model = glm::rotate(m_model, glm::radians(m_rotation[1]), glm::vec3(0.0, 1.0, 0.0));
		m_model = glm::rotate(m_model, glm::radians(m_rotation[2]), glm::vec3(0.0, 0.0, 1.0));
		glm::mat4 mvp = *m_proj * *m_view * m_model;
		shader->setUniformMat4f("u_MVP", mvp);
	}

	void TestRenderColorCube::OnRender(Renderer& renderer)
	{
		(m_activeRotation[0]) ? m_rotation[0]++ : m_rotation[0];
		(m_activeRotation[1]) ? m_rotation[1]++ : m_rotation[1];
		(m_activeRotation[2]) ? m_rotation[2]++ : m_rotation[2];
		renderer.Clear();
		renderer.Draw(*va, *ib, *shader);
	}

	void TestRenderColorCube::OnImGuiRender()
	{
		ImGui::SliderFloat3("modelTranslation", &m_modelTranslation->x, 0.0f, W_WIDTH);
		ImGui::SliderFloat3("rotation", &m_rotation[0], 0.0f, 1000.0f);
		ImGui::Checkbox("activate x rotation", &m_activeRotation[0]);
		ImGui::Checkbox("activate y rotation", &m_activeRotation[1]);
		ImGui::Checkbox("activate z rotation", &m_activeRotation[2]);
	}

}