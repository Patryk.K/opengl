#pragma once
#pragma once
#include "test/Test.h"
#include "VertexDefinition.h"
#include "Renderer.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h""
#include "IndexBuffer.h"
#include "Shader.h"


namespace test {

	class TestRenderBoxMVP : public Test {
	public:
		TestRenderBoxMVP();
		~TestRenderBoxMVP();

		void OnUpdate(float deltaTime) override;
		void OnRender(Renderer& renderer) override;
		void OnImGuiRender() override;

	private:
		float vertex[8] = {
			   -50.0f, -50.0f,
			    50.0f, -50.0f,
			    50.0f,  50.0f,
			   -50.0f,  50.0f
		};

		unsigned int indicies[6]{
			 0,1,3,
			 3,2,1, // front
		};

		float m_ClearColor[4] = { 1.0f, 1.0f, 1.0f, 1.0f };

		glm::mat4* m_proj;
		glm::mat4* m_view;
		glm::vec3* m_modelTranslation;

		VertexArray* va;
		VertexBuffer* vb;
		VertexBufferLayout* layout;
		IndexBuffer* ib;
		Shader* shader;

	public:
		float W_HEIGHT = 540.0f;
		float W_WIDTH = 960.0f;

	};
}