#include "test/TestRenderBoxMVP.h"
#include "imgui/imgui.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

namespace test {


	TestRenderBoxMVP::TestRenderBoxMVP()
	{
		va = new VertexArray();
		vb = new VertexBuffer(vertex, sizeof(vertex));
		layout = new VertexBufferLayout();
		layout->Push<float>(2);
		va->AddBuffer(*vb, *layout);

		ib = new IndexBuffer(indicies, 6);
		shader = new Shader("res/shaders/BasicBox.shader");
		shader->Bind();

		m_proj = new glm::mat4(glm::ortho(0.0f, W_WIDTH, 0.0f, W_HEIGHT, -100.0f, 100.0f));
		m_view = new glm::mat4(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0)));

		m_modelTranslation = new glm::vec3(W_WIDTH/2, W_HEIGHT/2, 0.0f);

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	TestRenderBoxMVP::~TestRenderBoxMVP()
	{
		delete va;
		delete vb;
		delete layout;
		delete ib;
		delete shader;

		delete m_proj;
		delete m_view;
		delete m_modelTranslation;
	}

	void TestRenderBoxMVP::OnUpdate(float deltaTime)
	{
		glm::mat4 m_model = glm::translate(glm::mat4(1.0f), *m_modelTranslation);
		glm::mat4 mvp = *m_proj * *m_view * m_model;
		shader->setUniform4f("u_Color", m_ClearColor[0], m_ClearColor[1], m_ClearColor[2], m_ClearColor[3]);
		shader->setUniformMat4f("u_MVP", mvp);
	}

	void TestRenderBoxMVP::OnRender(Renderer& renderer)
	{
		renderer.Clear();
		renderer.Draw(*va, *ib, *shader);
	}

	void TestRenderBoxMVP::OnImGuiRender()
	{
		ImGui::ColorEdit4("Clear color", m_ClearColor);
		ImGui::SliderFloat3("modelTranslation", &m_modelTranslation->x, 0.0f, W_WIDTH);
	}
	
}