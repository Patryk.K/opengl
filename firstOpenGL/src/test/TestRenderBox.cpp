#include "test/TestRenderBox.h"
#include "imgui/imgui.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

namespace test {


	TestRenderBox::TestRenderBox()
	{
		va = new VertexArray();
		vb = new VertexBuffer(vertex, sizeof(vertex));
		layout = new VertexBufferLayout();
		layout->Push<float>(2);
		va->AddBuffer(*vb, *layout);
		ib = new IndexBuffer(indicies, 6);
		shader = new Shader("res/shaders/BasicBox.shader");
		shader->Bind();

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	TestRenderBox::~TestRenderBox()
	{
		delete va;
		delete vb;
		delete layout;
		delete ib;
		delete shader;
	}

	void TestRenderBox::OnUpdate(float deltaTime)
	{
		shader->setUniformMat4f("u_MVP", glm::mat4(1.0f));
		shader->setUniform4f("u_Color", m_ClearColor[0], m_ClearColor[1], m_ClearColor[2], m_ClearColor[3]);
	}

	void TestRenderBox::OnRender(Renderer& renderer)
	{
		renderer.Clear();
		renderer.Draw(*va, *ib, *shader);
	}

	void TestRenderBox::OnImGuiRender()
	{
		ImGui::ColorEdit4("Clear color", m_ClearColor);
	}

}