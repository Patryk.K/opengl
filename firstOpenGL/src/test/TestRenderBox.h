#pragma once
#include "test/Test.h"
#include "VertexDefinition.h"
#include "Renderer.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h""
#include "IndexBuffer.h"
#include "Shader.h"

namespace test {

	class TestRenderBox : public Test {
	public:
		TestRenderBox();
		~TestRenderBox();

		void OnUpdate(float deltaTime) override;
		void OnRender(Renderer& renderer) override;
		void OnImGuiRender() override;

	private:
		float vertex[8] = {
			 -0.5f, -0.5f,
			  0.5f, -0.5f,
			  0.5f,  0.5f,
			 -0.5f,  0.5f,
		};

		unsigned int indicies[6]{
			 0,1,2,
			 2,3,0
		};

		float m_ClearColor[4] = { 1.0f,1.0f,1.0f,1.0f };

		VertexArray* va;
		VertexBuffer* vb;
		VertexBufferLayout* layout;
		IndexBuffer* ib;
		Shader* shader;
	};
}