#include "ImguiTestWindow.h"

#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"
#include "test/TestClearColor.h"
#include "test/TestRenderBox.h"
#include "test/TestRenderBoxMVP.h"
#include "test/TestTexturedBoxMVP.h"
#include "test/TestRenderCube.h"
#include "test/TestRenderColorCube.h"
#include <algorithm>

namespace test {

	ImguiTestWindow::ImguiTestWindow(GLFWwindow* window, bool callbacks, const char* glsl_version, Renderer& renderer)
		:m_Renderer(renderer), items{"Display clear color scene", 
									 "Display render box scene",
									 "Display render box scene with MVP translation",
									 "Display textured box scene with MVP translation",
									 "Display render cube scene with MVP translation",
									 "Display render color cube scene with MVP translation" }
	{
		ImGui::CreateContext();
		ImGui::StyleColorsDark();
		ImGui_ImplGlfw_InitForOpenGL(window, true);
		ImGui_ImplOpenGL3_Init(glsl_version);
	}

	ImguiTestWindow::~ImguiTestWindow()
	{
		if (!testsVector.empty()) {
			delete testsVector.back();
			testsVector.clear();
		}
		ImGui_ImplOpenGL3_Shutdown();
		ImGui_ImplGlfw_Shutdown();
		ImGui::DestroyContext();
	}

	void ImguiTestWindow::testWindowNewFrame()
	{
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();
	}

	void ImguiTestWindow::testWindowRender()
	{
		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	}

	void ImguiTestWindow::testMainComponents()
	{
		setDefault();
		if (ImGui::BeginCombo("##combo", current_item))
		{
			for (int n = 0; n < IM_ARRAYSIZE(items); n++)
			{
				bool is_selected = (current_item == items[n]); 
				if (ImGui::Selectable(items[n], is_selected))
					current_item = items[n];
					addTestClass();
					if (is_selected){ 
						ImGui::SetItemDefaultFocus();
					}
						  
			}
			ImGui::EndCombo();
		}
	}

	void ImguiTestWindow::displayTest()
	{
		testsVector.back()->OnUpdate(0.0f);
		testsVector.back()->OnRender(m_Renderer);
		testsVector.back()->OnImGuiRender();
		if (ImGui::Button("Back", ImVec2(50, 20))) {
			current_item = NULL;
			delete testsVector.back();
			testsVector.pop_back();
		}
	}

	int ImguiTestWindow::getCurrentListItem()
	{
		if (current_item != NULL) {
			int n = sizeof(items) / sizeof(items[0]);
			auto itr = std::find(items, items + n, current_item);
			if (itr != std::end(items))
				return std::distance(items, itr);
		}
		return -1;
	}


	void ImguiTestWindow::addTestClass()
	{
		if (testsVector.size() < 1 && current_item != NULL) {
			switch (getCurrentListItem())
			{
				case 0: testsVector.push_back(createClass<TestClearColor>()); break;
				case 1: testsVector.push_back(createClass<TestRenderBox>()); break;
				case 2: testsVector.push_back(createClass<TestRenderBoxMVP>()); break;
				case 3: testsVector.push_back(createClass<TestTexturedBoxMVP>()); break;
				case 4: testsVector.push_back(createClass<TestRenderCube>()); break;
				case 5: testsVector.push_back(createClass<TestRenderColorCube>()); break;
			}
		}
	}

	void ImguiTestWindow::setDefault()
	{
		GLCall(glClearColor(0.0f,0.0f,0.0f,1.0f));
		GLCall(glClear(GL_COLOR_BUFFER_BIT));
	}

}