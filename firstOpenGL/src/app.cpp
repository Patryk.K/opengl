﻿#include "ImguiTestWindow.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <windows.h>


float W_HEIGHT = 540.0f;
float W_WIDTH = 960.0f;
float W_DEPTH = -300.0f;

int main(void)
{
	GLFWwindow* window;
	if (!glfwInit())
		return -1;
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow((int)W_WIDTH, (int)W_HEIGHT, "Hello World", NULL, NULL);
	glEnable(GL_DEPTH_TEST);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

	if (glewInit() != GLEW_OK) {
		std::cout << "Error" << std::endl;
	}

	std::cout << glGetString(GL_VERSION) << std::endl;


	{
		GLCall(glEnable(GL_BLEND));
		GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);

		Renderer renderer;

		test::ImguiTestWindow imguiTestWindow(window, true, "#version 410 core", renderer);

		while (!glfwWindowShouldClose(window))
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			imguiTestWindow.testWindowNewFrame();

			switch (imguiTestWindow.getCurrentListItem())
			{
				case -1: imguiTestWindow.testMainComponents(); break;
				default: imguiTestWindow.displayTest(); break;
			}

			imguiTestWindow.testWindowRender();

			GLCall(glfwSwapBuffers(window));
			GLCall(glfwPollEvents());
		}
	}
	glfwTerminate();
	return 0;
}