#shader vertex
#version 410 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 ColorCoord;

out vec4 v_ColorCoord;
uniform mat4 u_MVP;

void main()
{
    v_ColorCoord = ColorCoord;
    gl_Position = u_MVP * position;
};


#shader fragment
#version 410 core

layout(location = 0) out vec4 color;

in vec4 v_ColorCoord;

void main()
{
    color = v_ColorCoord;
};